# xFlat for Max OS X 
### Version: 1.0

首次运行需指定一个用于存放config文件的文件夹。

***config文件：***
*每一个特定的抓取目标（网站），都对应着一份特定的配置文件（config文件）。通过这种‘可配置’的方式，使得xFlat能够更加灵活地适应各种不同的网站。通常约定以目标网站的名称命名其config文件，比如'http://www.someweb.com/' 所对应的config文件为'someweb.js'。*

*config文件本质上是一个js脚本。它使用尽可能少的代码来定义要在目标网站上‘抓什么’，‘怎么抓’以及'如何显示'。用户可以根据自己的需要，自行创建特定的config文件，从而实现对特定网站的抓取。（详见本文最后'创建config文件'部分了解如何创建config文件）*

## 使用
1. 选择框左一：选择要运行的config（要抓取的网站）；
2. 选择框左二：选择目标网站的子分类 （如果存在）；
3. Page框： 设定要抓取的页面（或范围）。可以是单独的页号，如：2（只抓取第2页的内容）；也可以是页号范围如：1-10（抓取第1到第10页的所有内容 ）。默认值1；
4. Search框： 可选，指定关键字搜索。多关键字用空格分开。若忽略，则无关键字过滤，返回指定页面的所有结果；若填写，则只返回指定页面中匹配关键字的结果。例如：

	  	  ｛ Page： 3 ｝：返回第3页的所有结果；

		  ｛ Page： 3， Search：张三 ｝：返回第3页中匹配‘张三’ 的结果；

		  ｛ Page： 1-100， Search：张三 李四 ｝：返回1到100页中匹配‘张三’ 或 ‘李四’ 的结果。

5. 播放按钮（左1）: 开始执行抓取。（ 在填完Page或Search后直接敲击‘enter’键也将开始抓取 ）
6. 停止按钮（左1，仅当抓取执行期间）: 终止抓取。
7. 调试按钮（左2）: 打开调试面板，查看错误。
8. 设置按钮（左3）: 打开设置面板。
9. 终止按钮（左4）: 当抓取正在进行时，终止抓取（同停止按钮）; 当抓取停止时，终止正在加载的图片（如果有）。
10. 运行状态信息: ［  A ｜ B ｜ C ｜ D ］或 ［  A ｜ B ｜ D ］

  * A: 所有已加载的内容／内容总数 ／ 丢弃的内容数量（丢弃：出错或用户终止时发生）

  * B: 正在进行的index请求／正在排队的index请求 （注：index代表抓取的主页。例如可能是论坛的标题页。 ）

  * C: 正在进行的子内容请求／正在排队的子内容请求  （注：C不一定存在，具体要看运行的config有无加载子内容的需求。例如，在抓取论坛的数据时，如果只需要显示所有帖子的标题（标题信息是由index请求直接返回的），则无须加载任何子内容，即此时无C；但如果除标题外，还需要抓取具体帖子的某些内容，那么这里就需要向具体帖子的地址发起一个二次请求，以获取所需的数据（即子内容），那么此时C就是存在的。 ）

  * D: 正在下载的图片数量／正在排队的图片数量 


## 已知问题:

1. 若使用socks代理，请勿设置为全局模式。否者图片无法显示。
2. 如果开启了迅雷（或其它p2p下载工具），即便速度很慢，也会大幅影响xFlat的加载速度。（猜测是否是缓冲区被霸占，或者端口冲突？）

## TODOs:

* 添加http，https代理功能。
* 多线程。



# 创建config文件
config文件本质上是一个js脚本(node端运行)，若要自行创建，请确保你的config脚本输出(module.exports)一个符合下列格式的object。
```
module.exports = {
	targets: [
		{'name': 'Target 1', 'url': 'http://someurl1' },
		{'name': 'Target 2', 'url': 'http://someurl2' },
		...
	],
	
	debug: false,
	referer: 'http://someurl',
	encoding: 'utf8',
	selector: '#some-id .some-class',
	
	getSearchStr: function ( $element ) {
		return $element.text();
	},

	parse: function ( $element, item ) {
		item.addView( $element.text() );
	}
}
```

## Config Reference 

### *Properties* 

#### targets
Array，指定适用于该config的所有网址,通常是目标网站里的各个子栏目。例如：
```
[
	{'name': 'Comedy Movies', 'url': 'http://someurl/comedy?page={pagenum}' },
	{'name': 'Action Movies', 'url': 'http://someurl/action?page={pagenum}' }
]
```
使用{pagenum}代替url中表示页号的数字。

#### debug [optional]
Bool, 是否调试该config，通常用于正在创建和编辑config时，默认为false。若为true，则在运行该config时，调试面板将自动开启，并且将显示额外调试信息如抓取的网页标题，抓取的子元素数量等...

### referer
String, 一个url地址，用于指定请求中表头header的referer属性。这是为了向目标服务器伪装请求的来源，防止被对方屏蔽。详情请搜索request的header属性。

### encoding [optional]
String, 目标页面的编码，用于对网页进行解码。默认为'utf8', 某些中文网站可能为'GBK'。其它支持的编码参见：https://github.com/ashtuchkin/iconv-lite

### selector
在目标页面上提取所需信息的jquery选择器。selector可以是string，如'#some-id .some-class', 也可以是function。 若为function,  需接收一个全局jquery对象作为参数，并返回选择后的jquery对象，例如下面两个selector是等效的：
```
//string
selector: '#some-id .some-class', 

// or function
selector: function ( $ ) { 
    return $('#some-id .some-class')
}
```
注意：xFlat后端对jquery选择器的实现基于cheerio(https://github.com/cheeriojs/cheerio)，它不支持某些伪类选择器例如':eq(n)'等。 因此，若有此类需求，请使用function形式的selector，并使用cheerio支持的.eq(n)方法来达成此目的。例如：
```
//won't work, since ':eq(2)' is not supported.
selector: '#some-id .some-class tr:eq(2)', 

//OK :)
selector: function ( $ ) { 
    return $('#some-id .some-class tr').eq(2);
}
```
### *Methods* 

### getSearchStr ( $element )
返回一个用于关键字匹配的字符串。参数$element是一个jquery对象，它是xFlat在目标网页上，通过上述selector已提取到的所有目标元素（jquery对象）的其中一个子对象。在用户输入了关键字的情况下，xFlat将使用此处返回的字符串与关键字进行匹配，以判断该$element是否是一个符合用户搜索的结果。

### parse: function ( $element, item )
该方法决定了如何在xFlat中显示所需的内容。
参数$element同上，并且是已经通过了关键字匹配的（如果有）。参数item是xFlat为该$element分配的一个显示项。
你此时需要做的是，按照你的需要，摘取$element上的相关内容，然后通过item.addView(...)方法把这些内容提交给item，以便xFlat能够将它们正确的显示出来。请参见下面的例子：
```
{
    ...
    ...
    
	parse: function ( $element, item ) {
	
    	//get the text of the $element
		var copy = $element.text(); 
		
		//get the href of the $element
		var href = $element.attr('href');
		
		//add 3 contents into item
		item.addView( 'text', copy );
		item.addView( 'img', href, '#some-id some-class img', getImgSrc, 1 );
		item.addView( 'text', href, '#some-id some-class li', getDownloadCopy, Infinity );
		
		// set click tag of the item
		item.setCTA( href );
	}
}

// Helper functions
var getImgSrc = function ( $e ) {
	return $e.attr('src');
};

var getDownloadCopy = function ( $e ) {
	return $e.text();
}
```
上面的代码中，总共为item添加了3个显示部分，分别是：
```
item.addView( 'text', copy ); // 显示$element的文本内容；
```
```
item.addView( 'img', href, '#some-id some-class img', getImgSrc, 1 ); // 前往href这个地址（通常是具体的子页面），搜寻某个内容（第三个参数为选择器，这里是在找一张图片），并显示。
```
```
item.addView( 'text', href, '#some-id some-class li', getDownloadCopy, Infinity ); // 同上，前往子页面搜寻一段文本，并显示。
```
注意：item.addView的调用顺序决定了显示的上下顺序。

### item.addView( type, content )
* type: 内容类型，目前支持'text'或'img';
* content: String，需显示的具体内容。若type为'img',则此处应该为图片的src.

### item.addView( type, url, selector, getValueCallback, maxCount )
显示远程页面的内容。

* type: 内容类型，可以是'text'或'img';

* url: 远程页面的地址。支持相对路径;

* selector: 在远程页面上抓取特定内容的选择器;

* getValueCallback: 一个function，以selector选择的对象为参数，返回具体的内容。（参见上例中注释'Helper functions'后的两个方法）;

* maxCount: 若selector返回的内容为多条，则需指定最大的显示数量。默认为1。 如需全部显示，请指定maxCount为Infinity。