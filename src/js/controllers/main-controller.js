define(['jquery', 
		'../views/pin-list-view', 
		'../views/ctl-view',
		'../views/preference-view', 
		'../core/pin-loader', 
		'../common/logger',
		'bootstrap',
		], function ( $, PinListView, CtlView, PreferenceView, PinLoader, Logger ) {

	var SharedGlobal = requireNode('./js/core/shared-global');

	var maxItemLoadingCount = 6;

	var proxy = requireNode('./js/core/img-proxy');

	var exec = requireNode('child_process').exec;

	var DLoader = requireNode('./js/core/data-loader');

	var cfgManager = requireNode('./js/core/config-manager');

	var MainController = function () {

		this.pinListView = new PinListView(  $('#pin-list') );

		this.pinListModel = requireNode( './js/models/pin-list-model' );

		this.preferenceModel = requireNode( './js/models/preference-model' );

		this.ctlView = new CtlView( $('#user-control') );

		this.prefrenceView = new PreferenceView($('#preference'), SharedGlobal.validateDir );

		this.itemPendingQueue = [];

		this.itemLoadingQueue = [];

		this.userStopped = true;


		this.pinListModel.onItemComplete = function ( spiderItem ) {

			if( this.userStopped ) return;

			this.itemPendingQueue.push( spiderItem );

			this.popPendingQueue();

		}.bind( this );


		this.onItemLoadComplete = function ( loader ) {

			var index = this.itemLoadingQueue.indexOf( loader );

			if( index !== -1 ) {

				this.itemLoadingQueue.splice( index, 1 );

				this.pinListView.addItem( loader.data );

				this.popPendingQueue();
			}

		}.bind(this );


		this.logInfo = function () {

			this.ctlView.logInfo(' [ ' + this.pinListView.totalItems + '/' + this.pinListModel.getDataCounterInfo() + ' | ' + this.pinListModel.getLoaderInfo() + ' | ' + this.itemLoadingQueue.length + '/' + this.itemPendingQueue.length + ' ]');

			setTimeout( this.logInfo, 100 );

		}.bind( this );

		this.resize = function () {

			this.pinListView.width =  $('body').innerWidth();

			this.pinListView.height = window.innerHeight - 123;

		}.bind( this );

		this.bindEvents();

		this.resize();

		this.logInfo();
	
		this.readConfigs();	

	}

	MainController.prototype = {

		constructor: MainController,

		get sysBlocked () {

			return this._sysBlocked;
		},

		set sysBlocked ( value ) {

			if( this._sysBlocked !== value ){

				this._sysBlocked = value;

				this.updateEnabled();
			} 
		},

		get sysStarted () {

			return this._sysStarted;
		},

		set sysStarted ( value ) {

			if( this._sysStarted !== value ) {

				this._sysStarted = value;

				this.ctlView.setStopBn( value );

				this.updateEnabled();
			}
		},


		updateEnabled: function () {

			this.ctlView.controlEnabled = !this._sysBlocked;

			this.ctlView.formEnabled = !this._sysBlocked && !this._sysStarted;
		},


		readConfigs: function () {

			this.sysBlocked = true;

			this.preferenceModel.readConfigs( function ( err, files ) {

				if( err ) {

					this.openPreference();
				
				}else {

					var labels = cfgManager.reset( files );

					this.ctlView.updateConfigList( labels );

					this.ctlView.$configList.trigger('change');
				}

				this.sysBlocked = false;

			}.bind( this ) );	
		},

		openPreference: function () {

			this.prefrenceView.open( this.preferenceModel.configDir );
		},

		openDebugTool: function () {

			SharedGlobal.getGui().Window.get().showDevTools();
			
		},

		start: function (cfg, pageStart, pageEnd, keywords ) {

			this.userStopped = false;

			proxy.clear();

			for(var i = 0, l = this.itemLoadingQueue.length; i < l; ++ i){

				this.itemLoadingQueue[i].isGC = true;

			}

			this.itemLoadingQueue.length = 0;
		
			this.pinListView.clear();

			this.sysStarted = true;

			if(cfg.debug) this.openDebugTool();

			this.pinListModel.start( cfg, pageStart, pageEnd, keywords );
		},

		stop: function () {

			this.userStopped = true;

			this.sysStarted = false;

			this.pinListModel.stop();

			this.itemPendingQueue.length = 0;

			for(var i = 0, l = this.itemLoadingQueue.length; i < l; ++ i){

				this.itemLoadingQueue[i].isGC = true;

			}

			this.itemLoadingQueue.length = 0;

			proxy.clear();
		},


		openChrome: function ( url ) {

			var chromePath = '/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome';

			var cmd = chromePath + ' --incognito ' + url;

			exec(cmd, function(error, stdout, stderr) {

				if(error)console.error(error);

			});
		},

		bindEvents: function () {

			var self = this;

			this.ctlView.$configList.on( 'change', function (e ) {

				// update config target select options

				var selectedIndex = $(this)[0].selectedIndex;

				if( selectedIndex === -1 ){

					self.ctlView.updateConfigTargetList( null );
				
				} else {

					var cfg = cfgManager.get( selectedIndex );

					if( cfg ) {

						if( cfg === cfgManager.errorCfg ){

							self.openDebugTool();

							return;
						}

						var labels = [];

						for(var i = 0, l = cfg.targets.length; i < l; ++ i) {

							labels.push( cfg.targets[i].name );
						}

						self.ctlView.updateConfigTargetList( labels );

					} else {

						console.log( 'cfg not found' );

						self.readConfigs();
					}
				}
			});

			this.ctlView.$container.on('submit', function ( e ) {

				e.preventDefault();

				var dataArray = self.ctlView.serializeArray();

				var cfg,
					cfgTargetIndex = -1,
					pageStr, 
					searchStr, 
					data, 
					tempArray, 
					p1, 
					p2,
					keywords = [];

				for(var i = 0, l = dataArray.length; i < l; ++ i) {

					data = dataArray[i];

					switch( data.name ){

						case 'config':

							cfg = cfgManager.get( Number(data.value) );

							if( cfg === cfgManager.errorCfg ){

								self.openDebugTool();

								return;
							}

							break;

						case 'configtarget':

							cfgTargetIndex = Number( data.value );

							break;

						case 'page':

							pageStr = data.value;

							if( !pageStr || !pageStr.length )pageStr = '1';

							break;

						case 'keywords':

							searchStr = data.value;

							break;
					}
				}

				if( !cfg || !cfg.targets || cfgTargetIndex < 0 || cfgTargetIndex >= cfg.targets.length )return;

				else cfg.targetIndex = cfgTargetIndex;

				tempArray = pageStr.split('-');

				p1 = Number( tempArray[0] );

				if(tempArray.length > 1)p2 = Number( tempArray[1] );

				else p2 = p1;

				tempArray = searchStr.split(/\s+/);

				for( var i = 0, l = tempArray.length; i < l; ++ i) {

					if( tempArray[i].length && keywords.indexOf(tempArray[i]) === -1 ) keywords.push( tempArray[i] );

				}

				self.start(cfg, p1, p2, keywords );
			});

			this.ctlView.$ctlBn.on('click', function ( e ) {

				var $this = $(this);

				if( $this.hasClass('disabled') ) return;

				if( $this.hasClass('log-bn') ){

					self.openDebugTool();

				} else if( $this.hasClass('glyphicon-play') ) {

					self.ctlView.$container.trigger('submit');

				} else if( $this.hasClass('glyphicon-stop') ) {

					self.stop();

				} else if( $this.hasClass('preference-bn') ) {

					self.openPreference();

				} else if( $this.hasClass('clear-bn')) {

					if(self.sysStarted )self.stop();

					else {

						self.itemPendingQueue.length = 0;

						proxy.clear();

					}
				}

			});

			this.prefrenceView.$container.on('setconfigfolder', function ( e, dir ) {

				if( self.preferenceModel.updateConfigDir( dir ) ){

					self.prefrenceView.close();

					self.readConfigs();

				}

			});


			this.pinListModel.on('complete', function () {

				this.sysStarted = false;

			}.bind( this ));


			this.pinListView.$container[0].addEventListener('itemdblclick', function ( e ) {

				if( e.data.clickTag )this.openChrome( e.data.clickTag );

			}.bind( this ), false );

			window.addEventListener('resize', this.resize, false );

			window.addEventListener('keydown', function (e) {

				if( e.target != document.body )return;

				switch(e.keyCode) {

					case 32:

						var $item = this.pinListView.$selectedItem;

						if( $item ) {

							e.preventDefault();

							// SharedGlobal.getGui().Shell.openExternal( $item.data('url') );
							if( $item.data('clickTag') ) this.openChrome( $item.data('clickTag') );
						}

						break;
				}

			}.bind(this), false);

		},

		popPendingQueue: function () {

			while( this.itemLoadingQueue.length <  maxItemLoadingCount && this.itemPendingQueue.length ) {

				var data = this.itemPendingQueue.shift();

				var loader = new PinLoader( data, this.onItemLoadComplete );

				this.itemLoadingQueue.push( loader );
			}
		},

	}

	return MainController;
});