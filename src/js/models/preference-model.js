
var localStorage = window.localStorage;

var SharedGlobal = require('../core/shared-global');

var configFileReg = /\.js$/;

var PreferenceModel = function () {

}

PreferenceModel.prototype = {

	get configDir () {

		return localStorage.getItem( '_xflat_config_dir' );

	},

	updateConfigDir: function ( dir ) {

		if( SharedGlobal.validateDir( dir ) ) {

			localStorage.setItem( '_xflat_config_dir', dir );

			return true;
		
		}

		return false;
	},


	readConfigs: function ( cb ) {


		var dir = this.configDir;

		if( dir && dir.length > 0 ){

			SharedGlobal.readdir( dir, cb, [ configFileReg ] );

		} else {

			setTimeout(function () {

				cb( new Error('config dir not set'));

			}, 1 );
		}

	}

}

module.exports = new PreferenceModel();
