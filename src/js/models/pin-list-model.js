
var RequestOpt = require('../core/request-opt');

var cheerio = require('cheerio');

var util = require("util");

var EventEmitter = require("events");

var DataLoader = require('../core/data-loader');

var SpiderContext = require('../core/spider-context');

var SpiderItem = require('../core/spider-item');

var proxy = require( '../core/img-proxy' );

proxy.startServer();


var PinListModel = function () {

	EventEmitter.call( this );

	this.loaders = [
		new DataLoader( 15, 3, 3000 ), // loader for index
	]

	this.onItemComplete = undefined;

	this.keywords = null;

	this.itemTotal = 0;

	this.discardedUrls = [];

	this.interrupted = true;
}

util.inherits( PinListModel, EventEmitter );



PinListModel.prototype.getDataCounterInfo = function () {

	return this.itemTotal + '/' + this.discardedUrls.length;
}

PinListModel.prototype.getLoaderInfo = function () {

	var str = this.loaders[0].getInfo();

	for(var i = 1, l = this.loaders.length; i < l; ++ i) {

		str = str.concat( ' | ' + this.loaders[i].getInfo() );

	}

	return str;
}



PinListModel.prototype.start = function ( config, pageStart, pageEnd, keywords ) {

	for(var i = 0, l = this.loaders.length; i < l; ++ i) {

		this.loaders[i].reset();
	}

	this.itemTotal = 0;

	this.discardedUrls.length = 0;

	// new session
	if( keywords && keywords.length ){

		this.keywords = [];

		for(var i = 0, l = keywords.length; i < l; ++ i) {

			this.keywords.push( new RegExp(keywords[i], 'i') );
		}

	} else {

		this.keywords = null;

	}

	this.interrupted = false;

	if( pageStart <= pageEnd ) {

		for( var i = pageStart; i <= pageEnd; ++ i) {

			this._loadIndex( config, i );

		}

	}else {

		for( var i = pageStart; i >= pageEnd; -- i) {

			this._loadIndex( config, i );
		}
	}
}

PinListModel.prototype.stop = function () {

	this.interrupted = true;

	for(var i = 0, l = this.loaders.length; i < l; ++ i) {

		this.loaders[i].clear( true );
	
	}

}


PinListModel.prototype.indexLoader = function () {

	return this.loaders[ 0 ];
}

PinListModel.prototype.dataLoader = function () {

	if(!this.loaders[ 1 ] )this.loaders[ 1 ] = new DataLoader( 5, 3, 4000 );

	return this.loaders[ 1 ];
}


PinListModel.prototype._loadIndex = function ( cfg, pageIndex ) {

	var url = cfg.targets[ cfg.targetIndex ].url.replace(/\{pagenum\}/, pageIndex + '');

	var referer = cfg.referer;

	var ctx = new SpiderContext( url, referer, cfg.encoding );

	var opt = RequestOpt.generate(  ctx.url, { 'Referer': ctx.referer } );

	var self = this;

	this.indexLoader().get( opt, ctx.encoding, function ( err, body ) {

		if( err ) {

			// loader error
			self._endIndexLoading( ctx, err );

			return;

		}else{

			if( self.interrupted )return;

			var $ = cheerio.load( body );

			var $element;

			if( typeof( cfg.selector ) === 'function' ){

				$element = cfg.selector( $ );

			}else{

				$element = $( cfg.selector );
			}

			if( cfg.debug ) {

				console.log('Index Loaded: ',$('title').text());

				console.log('Found Index Elements: ' + $element.length );

			}

			$element.each( function( i ) {

				var $this = $( this );

				if( self.filter( cfg.getSearchStr( $this ) ) ) {

					var item = new SpiderItem( url );

					cfg.parse( $this, item );

					self.itemTotal ++;

					if( item.pendingTaskCount > 0 ) {

						for( var taskUrl in item.pendingTasks ) {

							self._loadData( item, taskUrl, item.pendingTasks[ taskUrl ], ctx.encoding );

						}

					} else {

						self.commitItem( item );
					}

					item.pendingTasks = null;

				}

			});

			self._endIndexLoading( ctx );
		}
	})
}



PinListModel.prototype._endIndexLoading = function ( ctx, err ) {

	if ( err ) {

		console.error('index error: ', err.message );

		this.discardedUrls.push( ctx.url );

	} 

	this.checkComplete();
}


PinListModel.prototype.commitItem = function ( item ) {


	if( !item.isEmpty() && this.onItemComplete !== undefined ){

		this.onItemComplete( item );
	
	}

	this.checkComplete();
}



PinListModel.prototype.filter = function ( str ) {

	if( !this.keywords )return true;

	for( var i = 0, l = this.keywords.length; i < l; ++ i ) {

		var reg = this.keywords[i];

		if(reg.exec( str ) ) {

			return true;
		}
	}

	return false;
}

PinListModel.prototype._loadData = function ( item, url, tasks, encoding ) {

	var opt = RequestOpt.generate( url, { 'Referer': item.parentUrl } );

	var self = this;

	this.dataLoader().get( opt, encoding, function ( err, body ) {

		item.pendingTaskCount --;

		if( err ) {

			// net work error
			console.error( 'data error: ' + err.message );

			self.discardedUrls.push( url );

			for( var i = 0, l = tasks.length; i < l; ++ i ) {

				item.removeViewByData( tasks[i].data );

			}

			if( item.pendingTaskCount === 0 ) self.commitItem( item );

		} else {

			var $ = cheerio.load( body );

			for( var i = 0, l = tasks.length; i < l; ++ i) {

				var task = tasks[i];

				var viewData = tasks[i].data;

				var $element = $( tasks[i].selector );

				viewData.value = [];

				var count = 0;

				$element.each( function ( ) {

					var $this = $( this );

					var src = task.getValue( $this );

					if( src ) {

						if(viewData.type === 'img') {

							viewData.value.push( proxy.replaceUrl( src, {

						'refe': window.btoa( url )} ));


						} else {

							viewData.value.push( src );

						}

						if( (++ count) >= viewData.maxCount ) return false;

					}

				});
			}

			if( item.pendingTaskCount === 0 ) self.commitItem( item );

		}

	});
}


PinListModel.prototype.checkComplete = function ( ) {

	for(var i = 0, l = this.loaders.length; i < l; ++ i ) {

		if(!this.loaders[i].empty())return;
	}

	this.emit( 'complete' );
}

module.exports = new PinListModel();

