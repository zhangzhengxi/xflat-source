define([], function() {

	var _t = Date.now();

	var _ts = _t;

	var Logger = {

		log: function ( mes ) {

			console.log('_' + ( Date.now() - _t ) + 'ms (' + ( Date.now() - _ts ) + 'ms): ', mes );

			_ts = Date.now();
		},

		warn: function ( mes ) {

			console.warn('_' + ( Date.now() - _t ) + 'ms (' + ( Date.now() - _ts ) + 'ms): ', mes );

			_ts = Date.now();
		},

		error: function ( mes ) {

			console.error('_' + ( Date.now() - _t ) + 'ms (' + ( Date.now() - _ts ) + 'ms): ', mes );

			_ts = Date.now();

		},

	}

	return Logger;

});