
// Store informations of a index page search

var SpiderContext = function ( url, referer , encoding ) {

	this.url = url;

	this.referer = referer;

	this.encoding = encoding;
}

SpiderContext.prototype = {

	constructor: SpiderContext,
}

module.exports = SpiderContext;