var path = require('path');

var cachedUris = [];

var cachedLabels = [];

module.exports = {

	errorCfg: '_errcfg_',

	clear: function () {

		for( var i = 0, l = cachedUris.length; i < l; ++ i ) {

			try {

				delete require.cache[ require.resolve( cachedUris[ i ] ) ];

			}catch ( e ) {

			}
			
		};

		cachedUris.length = 0;

		cachedLabels.length = 0;
	},

	reset: function ( files ) {

		this.clear();

		for(var i = 0, l = files.length; i < l; ++ i) {

			cachedUris.push( files[ i ] );

			cachedLabels.push( path.basename( files[ i ], '.js') );
		}

		return cachedLabels.slice();
	},

	//return cfg, null if not found or this.errorCfg if there is an error in cfg

	get: function ( index ) {

		if( index < 0 || index >= cachedUris.length )return null;

		try {

			return require( cachedUris[ index ] );

		}catch ( e ) {

			if( e.code !== 'MODULE_NOT_FOUND' && e.code !== 'ENOENT') {

				console.error( 'config has error: ' , e );

				return this.errorCfg;
			}

			return null;
		}
	}

}