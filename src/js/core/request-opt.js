var _url = require('url');

var Socks5Agent = {
	'http:': require('socks5-http-client/lib/Agent'),
	'https:': require('socks5-https-client/lib/Agent'),
}

var App = require('./shared-global').getGui().App;

var headerTemp = {

	'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',

	'Accept-Encoding':'gzip, deflate, sdch',

	'Accept-Language':'en-US,en;q=0.8,zh-CN;q=0.6,zh-TW;q=0.4',

	'Cache-Control':'no-cache',

	'Connection':'keep-alive',

	'Pragma':'no-cache',

	'Upgrade-Insecure-Requests':'1',

	'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36'
}


function setAgent( opt, type, value ) {

	var protocol = _url.parse(opt.url).protocol;

	switch ( type ) {

		case 'SOCKS5':

			var a = null;

			if( value && ( a = value.split(':') ).length === 2 && Socks5Agent[ protocol ] ) {

				// console.log( 'set socks agent: ', protocol );

				opt.agentClass = Socks5Agent[ protocol ];

				opt.agentOptions = {

					socksHost: a[0],

        			socksPort: a[1]
				}

				// if(protocol === 'https:')opt.strictSSL = true;

				return true;
			}

			break;
	}

	return false;
}

module.exports = {

	generate: function ( path, headerOpt ) {

		var opt = {
			url: path,
			gzip : true,
			headers: { }
		};


		for(var p in headerTemp ) {

			opt.headers[ p ] = headerTemp[ p ];

		}

		if( headerOpt ) {

			for(var p in headerOpt ) {

				opt.headers[ p ] = headerOpt[ p ];

			}
		}

		//agent

		var proxyStr = App.getProxyForURL( path );

		if(	proxyStr !== 'DIRECT' ) {

			var proxies = proxyStr.split(/\s*;\s*/);

			for(var i = 0, l = proxies.length; i < l; ++ i) {

				var params = proxies[i].split(/\s+/);

				if( setAgent( opt, params[0], params[1] ) )break;
			}

		}

		return opt;

	}

}