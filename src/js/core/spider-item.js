var _url = require('url');

var SpiderItem = function ( parentUrl ) {

	this.parentUrl = parentUrl;

	this.viewDatas = [];

	this.pendingTasks = {};

	this.clickTag = undefined;

	this.pendingTaskCount = 0;
}



SpiderItem.prototype = {

	constructor: SpiderItem,

	_resolveSubUrl: function ( url ) {

		if( !_url.parse( url ).protocol ) {

			return _url.resolve( this.parentUrl , url );

		}

		return url;
	},

	setCTA: function ( url ) {

		this.clickTag = this._resolveSubUrl( url );
	},


	isEmpty: function () {

		return this.viewDatas.length === 0;
	},


	addView: function ( type, url, selector, getValue, maxCount ) {

		var data = { 

			'type': type, 

			'maxCount': maxCount !== undefined ? maxCount : 1 

		};

		this.viewDatas.push( data );

		if( selector === undefined ) {

			data.value = url;
		
		} else {

			url = this._resolveSubUrl( url );

			var pendingTask = {

				'selector': selector,

				'data': data,

				'getValue': getValue,

			};

			if( !this.pendingTasks[ url ] ) {

				this.pendingTaskCount ++;

				this.pendingTasks[ url ] = [ pendingTask ];

			}else {

				this.pendingTasks[ url ].push( pendingTask );
			}

		}

	},

	removeViewByData: function ( data ) {

		var index = this.viewDatas.indexOf( data );

		if( index !== -1 ){

			this.viewDatas.splice( index, 1 );
		}
	}

}


module.exports = SpiderItem;