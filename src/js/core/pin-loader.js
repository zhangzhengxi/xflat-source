define([], function () {
	
	var PinLoader = function ( data, callback ) {

		this.isGC = false;

		this.data = data;

		this.loadedCount = 0;

		this.loadTotal = 0;

		this.callback = callback;

		var viewDatas = data.viewDatas;

		for( var i = 0, l = viewDatas.length; i < l; ++ i ) {

			if( viewDatas[i].type === 'img' ){

				var srcs = !Array.isArray( viewDatas[i].value ) ? [ viewDatas[i].value ] : viewDatas[i].value;

				viewDatas[i].value = [];

				for(var j = 0, jl = srcs.length; j < jl; ++ j) {

					var img = document.createElement( 'img' );

					if( srcs[j].substr(0, 5) == 'data:' ){

						img.src = srcs[j];

						viewDatas[i].value.push( img );

					} else {

						this.loadImg( img, srcs[j], viewDatas[i].value );

					}
				}

			}
		}

		if(this.loadTotal === 0 ) {

			var self = this;

			setTimeout(function() {

				if( self.isGC )return;

				self.onComplete();

			}, 1);
		}

	};


	PinLoader.prototype = {

		constructor: PinLoader,

		onComplete: function () {

			if( !!this.callback ){

				var cb = this.callback;

				this.callback = null;

				cb( this );
			}
		},

		loadImg: function ( img, src, output ) {

			var self = this;

			var onImgLoad = function ( e ) {

				e.target.removeEventListener( 'load', onImgLoad, false );
				e.target.removeEventListener( 'error', onImgError, false );

				if( self.isGC )return;

				self.loadedCount ++;

				output.push( e.target );

				if( self.loadedCount === self.loadTotal )self.onComplete();
			};

			var onImgError = function ( e ) {

				console.log('pinloader: img err');

				e.target.removeEventListener( 'load', onImgLoad, false );
				e.target.removeEventListener( 'error', onImgError, false );

				if( self.isGC )return;

				self.loadedCount ++;

				if(self.loadedCount === self.loadTotal )self.onComplete();
			};

			img.addEventListener( 'load', onImgLoad, false );

			img.addEventListener( 'error', onImgError, false );

			img.src = src;

			this.loadTotal ++;
		}

	}

	return PinLoader;
});