
var gui = window.requireNode('nw.gui');

var fs = require('fs');

var path = require('path');


if ( process.platform === "darwin" ) {

	var mb = new gui.Menu({type: 'menubar'});

	mb.createMacBuiltin('xFlat', {
		hideEdit: false,
	});

	gui.Window.get().menu = mb;
}

module.exports = {

	getGui: function () {

		return gui;
	},

	validateDir: function ( dir ) {

		try {

			var stats = fs.lstatSync( dir );

			return stats.isDirectory() && !stats.isSymbolicLink();

		}catch (e){

			return false;

		}

	},


	readdir: function () {

		var lstatFile = function ( filePath, cb ) {

			fs.lstat( filePath , function (err, stats ) {

				if( !err && stats.isFile() && !stats.isSymbolicLink() ) {

					cb( filePath );
				
				} else {

					cb( null );
				}

			});
		};

		return function ( dir, cb, filters ) { 

			if( typeof filters === 'string' ) filters = [ filters ];

			fs.readdir( dir, function ( err, files ) {

				if( err ) {

					cb( err );

				}else {

					var output = [], 
						fileName, 
						taskCompleteCount = 0,
						taskTotal = 0;

					var callback = function ( filePath ) {

						if( filePath )output.push( filePath );

						taskCompleteCount ++;

						if( taskCompleteCount === taskTotal ) cb( null, output );

					}

					for(var i = 0, l = files.length; i < l; ++ i) {

						fileName = files[ i ];

						for( var j = 0, jl = filters.length; j < jl; ++ j ) {

							if( filters[j].test( fileName ) ){

								taskTotal ++;

								lstatFile( path.join( dir, fileName ), callback );

								break;
							}
						
						}

					}
					
					if( taskTotal === 0 ) cb( null, output );
				}

			} );
		}

	}()

}