var fs = require('fs');

var http = require('http');

var url = require('url');

var request = require('request');

var RequestOpt = require('./request-opt');

var serverUrl = 'http://127.0.0.1';

var serverPort = 8880;

var timeout = 8000;

var tasks = [];


function createRemoteTask ( serverRes, taskUrl, referer ) {

    var opt = RequestOpt.generate( taskUrl, referer && referer.length ? { 

        'Referer': referer,

        'Host': url.parse(taskUrl).hostname

    } : {

        'Host': url.parse(taskUrl).hostname

    } );

    opt.timeout = timeout;

    opt.gzip = false;

    var task = request( opt ).on('error', function ( err ) {

        send404( serverRes );

        console.error( 'request error: ', err.message );

    }).on('response', function ( res ) {

        if( res.statusCode !== 200 ) {

            send404( serverRes );

            console.error( 'response error: ', res.statusCode );

            return;
        }

        if( 'content-length' in res.headers ) delete res.headers['content-length'];

        res.headers[ 'transfer-encoding' ] = 'chunked';

        serverRes.writeHead( res.statusCode, res.headers );

        res.on('data', function ( chunk ) {

            serverRes.write( chunk );
        
        }).on('end', function () {

            serverRes.end();

        }).on('error', function ( err ) {

            console.error( 'response stream error...', err.message );

            serverRes.end();

        });

    });

    serverRes.on('finish', function () {

        removeTask( task );
    });

    tasks.push( task );
}

function removeTask ( task ) {

    var index = tasks.indexOf( task );

    if(index !== -1 )tasks.splice( index, 1 );
}


function send404( res ) {

    res.writeHead( 404 );

    res.end();
}

module.exports = {

    startServer: function () {

        var self = this;

         http.createServer(function( req, res ) {

            var urlObj = url.parse( req.url, true );

            var redi = urlObj.query.redi;

            var refe = window.atob( urlObj.query.refe );

            if(!redi || redi.length === 0) {

                 send404( res );

            }else{

                createRemoteTask( res, redi , refe );

            }

        }).listen(8880, '127.0.0.1');

        console.log('server started...' );

    },

    clear: function () {

        for(var i = tasks.length - 1; i >= 0; -- i ) {

            tasks[i].abort();

            tasks[i].emit('error', new Error('userabort'));
        }

    },

    replaceUrl: function ( input, query ) {

        var output = url.parse( input, true);

        output = url.parse( serverUrl + ':' + serverPort, true );

        output.query.redi = input;

        if( query ) {

            for(var p in query ) {

                output.query[p] = query[p];
            }
        }

        return output.format();
    }

}

