
var util = require("util");

var EventEmitter = require("events");

var request = require('request');

var iconv = require('iconv-lite');

var defaultMaxConcurrency = 5;

var defaultMaxRetryCount = 3;

var defaultTimeOutMilliSeconds = 6000;


var getUniqueID = function () {

	var _uniq = 0, prefix = '_tid';

	return function ( ) {

		return prefix + ( ++ _uniq );
	};

}()


var releaseTask = function ( task ) {

	task.id = null;

	task.opt = null;

	task.cb = null;
}


var DataLoader = function ( maxConcurrency, requestMaxRetryCount, timeoutMillSeconds ) {

	EventEmitter.call( this );

	this.timeoutMillSeconds = timeoutMillSeconds || defaultTimeOutMilliSeconds;

	this.taskDict = {};

	this.totalTasks = 0;

	this.pendingTasks = [];

	this.reqs = []; // current running requests

	this.maxConcurrency = maxConcurrency || defaultMaxConcurrency;

	this.requestMaxRetryCount = requestMaxRetryCount !== undefined ? requestMaxRetryCount : defaultMaxRetryCount;

	this.popTaskTimeoutId = null;

	this.onTaskRequested = function ( tid, err, body ) {

		if( this.taskDict[ tid ] ) {

			var task = this.taskDict[ tid ];

			if( err ) {

				if( task.runTimes < this.requestMaxRetryCount ) {

					console.warn( '> task will retry: ' + (task.runTimes + 1) + ' ' + task.opt.url + ', tid: ' + tid+ ', err: ' + err.message );

					this.pendingTasks.unshift( tid );
				
				}else {

					var cb = task.cb;

					releaseTask( task );

					this._removeTask( tid );

					cb( err );

				}

			}else{

				var cb = task.cb;

				releaseTask( task );

				this._removeTask( tid );

				cb( null, body );
			}
		}

		if( this.popTaskTimeoutId ) {

			clearTimeout( this.popTaskTimeoutId );

			this.popTaskTimeoutId = null;
		}

		var self = this;

		this.popTaskTimeoutId = setTimeout( function () {

			self._popTasks();

		}, 100 );

	}.bind( this );

	this._updateRequired = false;

	this.updateTime = function () {

		this._updateRequired = false;

		var t = Date.now();

		for( var i = this.reqs.length - 1; i >= 0; -- i ) {

			var req = this.reqs[ i ];

			if( t - req.startTime > this.timeoutMillSeconds ) {

				req.abort();

				this.reqs.splice( i, 1 );

				var id = req.tid;

				req.tid = undefined;

				if( id ) this.onTaskRequested( id, new Error('timeout') );
			}
		}

		if( this.reqs.length ) this.requireTimeUpdate();

	}.bind ( this );

}

util.inherits( DataLoader, EventEmitter );


DataLoader.prototype.requireTimeUpdate = function () {

	if( !this._updateRequired ) {

		this._updateRequired = true;

		setTimeout( this.updateTime, 1000 );
	}

}

DataLoader.prototype.get = function ( reqOpt, encoding, callback ) {

	this.pendingTasks.push( this._addTask( reqOpt, encoding, callback ) );

	this._popTasks();
}

DataLoader.prototype._addTask = function ( reqOpt, encoding, callback ) {

	var tid = getUniqueID();

	this.taskDict[ tid ] = {

		encoding: encoding || 'utf8',

		opt: reqOpt,

		cb: callback,

		runTimes: 0
	}

	this.totalTasks ++;

	return tid;
}

DataLoader.prototype._removeTask = function ( tid ) {

	delete this.taskDict[ tid ];

	this.totalTasks --;
}

DataLoader.prototype._popTasks = function () {

	while( this.reqs.length < this.maxConcurrency && this.pendingTasks.length ) {

		this._runTask( this.pendingTasks.shift() );

	}
}

DataLoader.prototype._runTask = function ( tid ) {

	var task = this.taskDict[ tid ];

	task.runTimes ++;

	var self = this;

	var req = request( task.opt );

	req.tid = tid;

	req.startTime = Date.now();

	this.reqs.push( req );

	req.on('error', function ( err ) {

		// !!! check this in error...
		console.log('error... ', err.message, this.tid );

		var id = self._removeReq( this );

		if( id ) self.onTaskRequested( id, err );

	})

	.pipe( iconv.decodeStream( task.encoding ) )

	.collect(function( err, body ) {

		var id = self._removeReq( req );

		if( id ) self.onTaskRequested( id, err, body );

	});

	this.requireTimeUpdate();
}


DataLoader.prototype._removeReq = function ( req ) {

	var tid = req.tid;

	req.tid = undefined;

	var index = this.reqs.indexOf( req );

	if( index !== -1 ) {

		this.reqs.splice( index, 1 );
	}

	return tid;
}


DataLoader.prototype.reset = function () {

	this.clear( false );
}

DataLoader.prototype.clear = function ( triggerCallbacks ) {

	if( this.popTaskTimeoutId ) {

		clearTimeout( this.popTaskTimeoutId );

		this.popTaskTimeoutId = null;
	}

	this.pendingTasks.length = 0;

	var cbs = [];

	for (var tid in this.taskDict ) {

		cbs.push( this.taskDict[ tid ].cb );

		releaseTask( this.taskDict[ tid ] );
	}

	this.taskDict = {};

	this.totalTasks = 0;

	for( var i = 0, l = this.reqs.length; i < l; ++ i ) {

		this.reqs[i].abort();

		this.reqs[i].tid = undefined;
	}

	this.reqs.length = 0;

	if( !triggerCallbacks ) {

		cbs.length = 0;

		return;
	}

	for(var i = 0, l = cbs.length; i < l; ++ i) {

		cbs[i]( new Error('user aborted') );
	}
}

DataLoader.prototype.empty = function () {

	return this.totalTasks === 0;
}

DataLoader.prototype.getInfo = function () {

	return this.reqs.length + '/' + this.pendingTasks.length;
}

module.exports = DataLoader;
