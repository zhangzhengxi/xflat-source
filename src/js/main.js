
require.nodeRequire = window.requireNode;

require.config({

    paths: {

        'jquery': './vendors/jquery-2.1.4.min',

        'domready': './vendors/ready.min',

        'bootstrap': '../bootstrap/js/bootstrap.min'
    },

 });

requirejs( ['jquery', 'domready', './controllers/main-controller'], function($, domready, Controller ) {
    
    domready( function() {

        var controller = new Controller();

        // requireNode('./js/tests/request-decoding');
    });
   
});