define(['jquery', './pin-list-item', '../common/logger' ], function( $, PinListItem, Logger ) {

	var maxAllowdOverflowDistance = 400;

	var PinListView = function( $element ) {

		this.$element =$element;

		this.$container = $element.find( '.item-container' );

		this.$selectedItem = null;

		this.cachedItems = [];

		this.width = 0;

		this.height = 0;

		this.gridProps = {

			itemWidth: 360,

			rowSpace: 18,

			columnSpace: 16,

			minGridWidth: 0,

			minColumnCount: 1,

			columns: [],
		};

		this.canAddNewItems = false;

		var self = this;

		this.$element.on('scroll', function() {

			self.requireVisibilityUpdate();

		}).on('click', '.pin-list-item', function() {

			self.setSelectedItem( $(this) );

		}).on('dblclick', '.pin-list-item', function () {

			var e = new Event('itemdblclick');

			e.data = $(this).data();

			self.$container[0].dispatchEvent(e);
		});
	}


	PinListView.prototype = {

		constructor: PinListView,

		get totalItems () {

			return this.cachedItems.length;
			
		},

		addItem: function ( data ) {

			var item = new PinListItem( data, this.gridProps.itemWidth );

			this.cachedItems.push( item );

			var shortest = this.getShortestColumnIndex();

			this.canAddNewItems = false;

			if( this.gridProps.columns[shortest].h < this.getPageBottom() ) {

				this.requireVisibilityUpdate();
			}
		},

		setSelectedItem: function ( $item ) {

			if(this.$selectedItem != $item ){

				if( this.$selectedItem ){

					this.$selectedItem.removeClass('selected');

				}

				this.$selectedItem = $item;

				this.$selectedItem.addClass('selected');

			}
		},

		clear: function () {

			this.$selectedItem = null;

			for(var i = 0, l = this.cachedItems.length; i < l; ++ i) {

				this.cachedItems[i].$element.remove();
			}

			this.cachedItems.length = 0;

			for(var i = 0, l = this.gridProps.columns.length; i < l; ++ i) {

				this.gridProps.columns[i].h = 0;
			}

			this.canAddNewItems = false;

			this.$element[0].scrollTop = 0;

			this.$container[0].style.height = 0 + 'px';
		},

		requieUpdate: function () {

 			if( !this.__updateRequired ){

 				this.__updateRequired = true;

 				if( !this.updateHandler )this.updateHandler = this.update.bind( this );

 				requestAnimationFrame( this.updateHandler );
 			}
		},

		requireVisibilityUpdate: function () {

			this.__itemVisibilityDirty = true;

			this.requieUpdate();
		},

		requireGridSizeUpdate: function () {

			this.__gridSizeDirty = true;

			this.requieUpdate();
		},

		get height () {

			return this.__height;
		},

		set height ( value ) {

			if( this.__height !== value ) {

				this.__height = value;

				this.requireGridSizeUpdate();

				this.requireVisibilityUpdate();
			}
		},

		get width () {

			return this.__width;
		},

		set width ( value ) {

			if( this.__width !== value ) {

				this.__width = value;

				this.requireGridSizeUpdate();
			}
		},


		getShortestColumnIndex: function () {

			var h = this.gridProps.columns[0].h, index = 0;

			for(var i = 1, l = this.gridProps.columns.length; i < l; ++ i) {

				if( this.gridProps.columns[i].h < h ){

					h = this.gridProps.columns[i].h;

					index = i;
				}
			}

			return index;

		},

		getTallestColumnIndex: function () {

			var h = this.gridProps.columns[0].h, index = 0;

			for(var i = 1, l = this.gridProps.columns.length; i < l; ++ i) {

				if( this.gridProps.columns[i].h > h ){

					h = this.gridProps.columns[i].h;

					index = i;
				}

			}

			return index;

		},

		getPageBottom: function () {

			return this.$element[0].scrollTop + this.height;
		},


		update: function () {

			if( !this.__updateRequired ) return;

			this.__updateRequired = false;

			if( this.__gridSizeDirty ) {

				this.__gridSizeDirty = false;

				var columnCount = 0, 
					itemWidth = this.gridProps.itemWidth,
					columnSpace = this.gridProps.columnSpace,
					gridWidth;

				if( !this.gridProps.minGridWidth ){

					this.gridProps.minGridWidth = Math.round( itemWidth * this.gridProps.minColumnCount + columnSpace * (this.gridProps.minColumnCount - 1) );
				}

				if( this.width < this.gridProps.minGridWidth ){

					columnCount = this.gridProps.minColumnCount;

					gridWidth = this.gridProps.minGridWidth;
				
				} else{

					var tmpW = 0;

					while( tmpW + itemWidth <= this.width ) {

						columnCount ++;

						tmpW += itemWidth + columnSpace;
					}

					gridWidth = itemWidth * columnCount + columnSpace * (columnCount - 1);
				}

				this.$element[0].style.width = this.width + 'px';
				this.$element[0].style.height = this.height + 'px';

				this.$container[0].style.left = Math.round( ( this.width - gridWidth ) * 0.5 ) + 'px';
				this.$container[0].style.width = Math.round( gridWidth ) + 'px';

				if( columnCount !== this.gridProps.columns.length ){

					//reset column count
					this.gridProps.columns.length = 0;

					for(var i = 0; i < columnCount; ++ i) {

						this.gridProps.columns[i] = {h: 0};

					}

					for(var i = 0, l = this.cachedItems.length; i < l; ++ i) {

						var item = this.cachedItems[i];

						if( item.height ) {

							var shortest = this.getShortestColumnIndex();

							var x = ( itemWidth + this.gridProps.rowSpace ) * shortest;

							var y = this.gridProps.columns[shortest].h;

							item.setPosition(x, y);

							this.gridProps.columns[shortest].h += columnSpace + item.height;

						}else break;

					}

					this.$container[0].style.height = this.gridProps.columns[this.getTallestColumnIndex()].h + 'px';

					this.__itemVisibilityDirty = true;
				}

			}

			if( this.__itemVisibilityDirty ) {

				this.__itemVisibilityDirty = false;

				var pageBottom = this.getPageBottom();

				var pageTop = pageBottom - this.height;

				pageBottom += maxAllowdOverflowDistance;

				pageTop -= maxAllowdOverflowDistance;

				var items = this.cachedItems, item;

				var canAddNewItems = this.gridProps.columns[this.getShortestColumnIndex()].h < pageBottom;

				for( var i = 0, l = items.length; i < l; ++ i ) {

					item = items[i];

					if( item.height !== 0 ){

						if( item.y > pageTop - item.height && item.y < pageBottom) {

							if( !item.visible ) {

								item.$element.appendTo( this.$container );

								item.visible = true;
							}
						
						}else {

							if( item.visible ) {

								item.$element.detach();

								item.visible = false;
							}
						}

					} else if( canAddNewItems ) {

						this.pushNewItemToColumn( item );

						canAddNewItems = this.gridProps.columns[this.getShortestColumnIndex()].h < pageBottom;

					} else break;
				}

				if( canAddNewItems !== this.canAddNewItems){

					 this.canAddNewItems = canAddNewItems;

					 if( canAddNewItems ){

					 	this.$container[0].dispatchEvent(new Event('canaddnewitems'));
					 }
				}
			}

		},

		pushNewItemToColumn: function ( item ) {

			var itemWidth = this.gridProps.itemWidth;

			var shortest = this.getShortestColumnIndex();

			var x = ( itemWidth + this.gridProps.rowSpace ) * shortest;

			var y = this.gridProps.columns[shortest].h;

			item.setPosition(x, y);

			item.$element.appendTo( this.$container );

			item.visible = true;

			item.height = item.$element.height();

			this.gridProps.columns[shortest].h += this.gridProps.columnSpace + item.height;

			this.$container[0].style.height = this.gridProps.columns[this.getTallestColumnIndex()].h + 'px';
		}

	}

	return PinListView;
});