define(['jquery'], function( $ ) {


	var createFormGroup = function ( $e, validateHandler, validateDelay ) {

		return {

			$e: $e,

			$input: $e.find('input'),

			validateTimer: 0,

			validateDelay: validateDelay || 100,

			isError: false,

			isActive: false,

			validateHandler: validateHandler,

			active: function ( value ) {

				if( this.isActive !== value ) {

					this.isActive = value;

					if( value ) {

						this.error( false );

						if( this.validateHandler !== undefined ){

							this.$input.on('input', function () {

								clearTimeout( this.validateTimer );

								this.validateTimer = setTimeout( function () {

									this.error( !this.validateHandler( this.getValue() ) );

								}.bind( this ), this.validateDelay );

							}.bind( this ));
						} 

					}else {

						clearTimeout( this.validateTimer );

						this.$input.off();
					}
				}
			},

			getValue: function () {

				return this.$input[0].value;

			},

			setValue: function ( value ) {

				this.$input[0].value = value;
			},

			validate: function () {

				clearTimeout( this.validateTimer );

				this.error( !this.validateHandler( this.getValue() ) );

				return !this.isError;
			},


			error: function ( value ) {

				if( value !== this.isError ) {

					this.isError = value;

					if( value ) this.$e.addClass('has-error');

					else this.$e.removeClass('has-error');
				}
			}
		}

	};

	var PreferenceView = function ( $container, validateConfigDirHandler ) {

		this.$container = $container;

		this.opened = false;

		this.formConfig = createFormGroup( $container.find('.form-config-dir'), validateConfigDirHandler, 100 );

		this.$container.modal( { 'show': false, 'backdrop': 'static' } );

		this.commit = function () {

			var configDir = this.formConfig.getValue();

			if(configDir && configDir.length > 0 && this.formConfig.validate() ) {

				this.$container.trigger( 'setconfigfolder', configDir );

			}

		}.bind( this );
	}

	PreferenceView.prototype = {

		constructor: PreferenceView,

		open: function ( configDir ) {

			this.$container.modal( 'show' );

			this.$container.find('.form-group').removeClass('has-error');

			this.formConfig.setValue( configDir ? configDir : '' );

			if( !this.opened ) {

				this.opened = true;

				var self = this;

				this.$container.on('click', '.close', this.commit )

				.on('dragover', function (e) {

					e.stopPropagation();

					e.preventDefault();

				}).on('drop', function ( e ) {

					e.stopPropagation();

					e.preventDefault();

					var files = e.originalEvent.dataTransfer.files;

					if(files && files.length > 0) {

						self.formConfig.setValue( files[0].path );

						self.formConfig.validate();
					}

				});

				this.formConfig.active( true );
			}
		},


		close: function () {

			this.$container.modal( 'hide' );

			if( this.opened ) {

				this.opened = false;

				this.$container.off( 'click dragover drop' );

				this.formConfig.active( false );
			}
		}
	}

	return PreferenceView;

} );