define(['jquery'], function( $ ) {

	var CtlView = function ( $container ) {

		this.$container = $container;

		this.$configList = $( '#config-list' );

		this.$configTargetList = $( '#config-target-list' );

		this.$form = this.$container.find( '.target-form' );

		this.$input = this.$container.find('input');

		this.$fieldset = this.$container.find('fieldset');

		this.$info = this.$container.find('.info p');

		this.$ctlBn = this.$container.find('.ctl-bn');

		this.$palyPauseBn = this.$container.find('.bn-group .playpause-bn');

		this.$input.tooltip({

			'container': 'body',

			'placement': 'bottom',

			'trigger': 'hover'
		});

		this.formEnabled = true;

		this.controlEnabled = true;
	}

	CtlView.prototype = {

		constructor: CtlView,

		updateConfigList: function ( labels ) {

			this.$configList.empty();

			for(var i = 0, l = labels.length; i < l; ++ i) {
				this.$configList.append($('<option>' + labels[i] + '</option>').attr('value', i));
			}
		},

		updateConfigTargetList: function ( labels ) {

			this.$configTargetList.empty();

			if( !labels ) return ;

			for(var i = 0, l = labels.length; i < l; ++ i) {
				this.$configTargetList.append($('<option>' + labels[i] + '</option>').attr('value', i));
			}
		},

		setStopBn: function ( value ) {

			if( value ){

				this.$palyPauseBn.removeClass('glyphicon-play').addClass('glyphicon-stop');
			
			} else {

				this.$palyPauseBn.removeClass('glyphicon-stop').addClass('glyphicon-play');

			}
		},

		logInfo: function ( mes ) {

			this.$info[0].innerHTML = mes;

		},

		serializeArray: function () {

			return this.$form.serializeArray();
		},


		set formEnabled ( value ) {

			if( this._formEnabled !== value ) {

				this._formEnabled = value;

				if( value ) {

					this.$fieldset.attr('disabled', false);

				} else {

					this.$fieldset.attr('disabled', true);

					this.$input.tooltip('hide');

				}
			}
		},

		get formEnabled () {

			return this._formEnabled;
		},

		set controlEnabled ( value ) { 

			if( this._ctlEnabled !== value ) {

				this._ctlEnabled = value;

				if( value ) {

					this.$ctlBn.removeClass('disabled');

				}else {

					this.$ctlBn.addClass('disabled');

				}
			}
		},

		get controlEnabled () {

			return this._ctlEnabled;
		}
	}

	return CtlView;

});