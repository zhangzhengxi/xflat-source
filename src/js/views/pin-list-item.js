define(['jquery' ], function( $ ) {

	var PinListItem = function ( data, width ) {

		this.build( data, width );

		if( data.clickTag ) this.$element.data( 'clickTag', data.clickTag );

		this.height = 0;

		this.x = 0;

		this.y = 0;

		this.visible = false;
	};

	PinListItem.prototype = {

		constructor: PinListItem,

		setPosition: function ( x, y ) {

			this.x = x;

			this.y = y;

			this.$element.css( { 'top': y + 'px', 'left': x + 'px' } );
		},

		build: function ( data, width ) {

			this.$element = $('<div class="pin-list-item"></div>');

			this.$element.css('width', width + 'px');

			var viewDatas = data.viewDatas;

			for(var i = 0, l = viewDatas.length; i < l; ++ i) {

				switch( viewDatas[i].type ) {

					case 'img': 

						var imgs = viewDatas[i].value, img, scale, $img;

						for(var j = 0, jl = imgs.length; j < jl; ++ j) {

							img = imgs[j];

							scale = width / img.width;

							img.width = width;

							img.height = img.height * scale;

							img.draggable = false;

							$img = $( img );

							$img.addClass('pin-thumbnail');

							this.$element.append( $img );
						}

						break;

					case 'text':

						var value =  Array.isArray( viewDatas[i].value ) ? viewDatas[i].value.join('<br><br>') : viewDatas[i].value;

						this.$element.append($('<div class="pin-copy"><p>' +  value + '</p></div>'));

						break;

					default:

						console.error('undefined display type: ', viewDatas[i].type );
						break;
				}
			}
		},
	}

	return PinListItem;

});