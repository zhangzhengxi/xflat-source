
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var NwBuilder = require('nw-builder');
var shellJs = require('shelljs');

var AUTOPREFIXER_BROWSERS = [
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios_saf >= 7',
  'android >= 4.4',
  'bb >= 10'
];

gulp.task('styles', function () {

  return gulp.src('less/*.less')

    .pipe($.changed('css', {extension: '.less'}))

    .pipe($.sourcemaps.init())
    .pipe($.less())
    .pipe($.autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe($.minifyCss({compatibility: '*'}))
    .pipe($.sourcemaps.write('./maps'))
    .pipe(gulp.dest('src/css'))
    .pipe($.size({title: 'styles'}));
});


gulp.task('nw-build', ['styles'], function () {

    var nw = new NwBuilder({
    	version: '0.12.3',
        files: './src/**',
        platforms: ['osx64'],
        macIcns: './icons/app.icns',
        macCredits: './about.html'
    });

    nw.on('log', function (msg) {
        $.util.log('nw-builder', msg);
    });

    return nw.build().catch(function (err) {
        $.util.log('nw-builder', err);
    });
});


gulp.task('run', ['nw-build'], function(){
	shellJs.exec('open ./build/xFlat/osx64/xFlat.app');
});


gulp.task('default', ['run']);